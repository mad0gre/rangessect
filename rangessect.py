#! /usr/bin/env python

from argparse import ArgumentParser


def _combine_ranges(ranges, acc=None, count=0, verbose=True):
    if len(ranges) == 0:
        if verbose:
            print(acc)
        return count + 1

    for i in range(int(ranges[0][0]), int(ranges[0][1]) + 1):
        if acc is None:
            acc = []
        curr = acc[:]
        curr.append(i)
        count = _combine_ranges(ranges[1:], curr, count, verbose)

    return count


def _create_arg_parser():
    parser = ArgumentParser(description="Calculate all combinations of input ranges")
    parser.add_argument('-c', '--count',
                        action='store_true',
                        default=False,
                        help="print total number of combinations instead of each of them")
    parser.add_argument('input', help="input file containing ranges to be processed")
    return parser


def _main():
    parser = _create_arg_parser()
    args = parser.parse_args()
    ranges = _parse_input(args.input)
    count = _combine_ranges(ranges, verbose=not args.count)
    if args.count:
        print(count)


def _parse_input(infile_name):
    infile = open(infile_name)
    return [line.rstrip().split() for line in infile.readlines()]


if __name__ == '__main__':
    _main()
