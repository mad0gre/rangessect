# rangessect
Generate all possible combinations for a list of ranges.

Check files `sample1.txt` and `sample2.txt` for the expected input format.


## Usage
From the project folder:

##### Linux
`./rangessect.py inputfile`

##### Linux or Windows
`python rangessect.py inputfile`


### Optional Arguments
- `-h`, `--help`: Display help message.
- `-c`, `--count`: Print total number of combinations instead of each of them.


## Requirements
Python 3 is installed on the environment.
